﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_Hierarchy
{
	public partial class NaviPage : ContentPage
	{

		public NaviPage ()
		{
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NaviPage)}:  ctor");
            InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, true);
        }
        //Function when the spyro button is clicked
        void spyroClick(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(spyroClick)}");
            //PushAsync pushes the spyro page onto stack
            Navigation.PushAsync(new spyroPage());
        }
        //Function when the crash button is clicked
        void crashClick(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(crashClick)}");
            //PushAsync pushes the crash page onto stack
            Navigation.PushAsync(new crashPage());
        }
        //Function when the ratchet button is clicked
        void ratchetClick(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(ratchetClick)}");
            //PushAsync pushes the ratchet page onto stack
            Navigation.PushAsync(new ratchetPage());
        }


    }
}