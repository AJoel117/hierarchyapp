﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_Hierarchy
{
	//[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PrevPage : ContentPage
	{
		public PrevPage (string prevPageString)
		{
			InitializeComponent ();
            lastPageLabel.Text = prevPageString;
		}
	}
}